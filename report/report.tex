%% 
%% Created in 2018 by Martin Slapak
%%
%% Based on file for NRP report LaTeX class by Vit Zyka (2008)
%%
%% Compilation:
%% >pdflatex report
%% >bibtex report
%% >pdflatex report
%% >pdflatex report

\documentclass{mvi-report}

\usepackage[utf8]{inputenc}

\title{Precipitation Video Resolution Upscaling}

\author{Matej Choma}
\affiliation{ČVUT - FIT}
\email{chomamat@fit.cvut.cz}

\def\file#1{{\tt#1}}

\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
Video resolution upscaling, also called super-resolution (SR), is a computer vision task that aims at recovering of upscaled
high-resolution (HR) video from a low-resolution (LR) one. An HR version of each frame has to be generated to achieve SR.

The image SR problem is ill-posed as there are multiple valid solutions for mapping from LR to HR space.
Image SR methods can be categorized as following \cite{shi2016real}:
\begin{itemize}
    \item \textit{multi-image SR} methods use multiple LR instances of the scene to produce one SR image containing details
    missing in separate LR images \cite{farsiu2004fast, sajjadi2018frame},
    \item \textit{single image SR (SISR)} uses only one LR image building on the assumption that there is some redundancy present in natural data. \cite{UnetCascade, SRGAN, cai2019ntire}.
\end{itemize}

SISR is a popular problem with many real-world applications. In recent years, NTIRE holds an image SR challenge and the methods are dominated by Convolutional Neural Networks (CNN) \cite{cai2019ntire}.

In this work, I explore deep learning SISR methods for $\times 4$ upscaling of weather radar precipitation video. The 6 seconds long video consists of 144 consecutive weather radar images of size $480\times 270$ captured above the Czech Republic (Figure \ref{fig:frame_base}).

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=\linewidth]{../imgs/frame.png}\vskip-0.5cm
  \caption{Frame of target weather radar video.}
  \label{fig:frame_base}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data}
Both weather radar and real-world photography data are used.

DIV2K \cite{DIV2K} is a dataset of diverse, high definition high-resolution images with corresponding LR instances obtained through various down-scaling operators. I have chosen $\times 4$ bicubic downscaled set. HR images of the training set are cropped into $480\times 480$ squares with stride $240$, and during training, a $128\times 128$ patch is randomly cropped from these. From validation HR images, a central patch of $1000\times 1000$ is extracted. 

Weather radar dataset consists of high-resolution OPERA programme composite images\footnote{data provided by Meteopress, \\https://www.meteopress.cz/radar/} from April to August 2019. From each HR composite image, observing rain, the rainiest $1000\times 1000$ patch is cropped. Corresponding LR images are computed via bicubic downscaling. During training, rainy $128\times 128$ patches are further cropped from the training set HR images.

All methods take input images scaled to $[0, 1]$ and produce SR image in the range $[-1, 1]$ as proposed by \cite{SRGAN}. Metrics are computed in the RGB space.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Methods}
I explore the use of two CNN architectures.

U-Net is a fully-convolutional network, implemented according to \cite{UnetCascade} (Figure \ref{fig:unet}) on the RCAB architecture \cite{RCAB}. Due to computational reasons, the number of features in the convolutional layers was reduced from $(64, 128, 256)$ to $(32, 64, 128)$. The number of cascades in both \textit{Main Network} and \textit{Cascading Block} was reduced to 3. The network was trained with the MSE loss.

SRGAN \cite{SRGAN} is a Generative Adversarial Network \cite{gans}, which consists of two networks -- generator SRResNet and discriminator (Figure \ref{fig:srgan}). No changes were made to the generator architecture and it was trained with MSE loss. One convolutional block with 64 output features was added to the discriminator in front of the \textit{Dense(1024)} layer, to reduce the size of the network. Generator was trained based on the following loss function:
$$
l^{SR} = l^{SR}_{MSE} + 10^{-3}\cdot\Sigma^N_{n=1}-\log D_{\theta_D}(G_{\theta_G}(I^{LR}))
$$
where discriminator $D_{\theta_D}$ predicts the probability that generated SR image $G_{\theta_G}(I^{LR})$ is an actual HR image. Discriminator loss function from the original paper \cite{gans} is used.

Networks were trained on single GPU NVIDIA RTX 2070 Super. Batch size is set to 16.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- VYSLEDKY
\section{Results}

U-Net and SRResNet were both trained for total of 50 epochs twice:
\begin{enumerate}
  \item from scratch on the weather radar dataset (\textit{U-Net, SRResNet}),
  \item pre-trained on the DIV2K dataset for 10 epochs and trained on weather radar dataset for the following 40 (\textit{U-Net DIV2K, SRResNet DIV2K}).
\end{enumerate}
SRGAN was trained for 50 epochs on the weather radar dataset with generator initiated by both \textit{SRResNet} and \textit{SRResNet DIV2K} after the 10 epoch pre-training.

The final models were selected according to the PSNR on the weather radar validation set. Following table show evaluation results on the weather radar test set, using metrics PSNR and SSIM. Example of predictions by all methods can be seen in Figure \ref{fig:pred} and \ref{fig:pred_small}.

\begin{table}[h!]
\begin{tabular}{l|lll}
               & \# params & PSNR           & SSIM           \\ \hline
Bicubic        & 0         & 23.28          & \textbf{0.772} \\
U-Net          & 3.6 M     & 24.11          & 0.414          \\
U-Net DIV2K    & 3.6 M     & 25.87          & 0.494          \\
SRResNet       & 1.55 M    & 26.05          & 0.163          \\
SRResNet DIV2K & 1.55 M    & \textbf{26.08} & 0.149          \\
SRGAN          & 23.3 M    & 23.77          & 0.112          \\
SRGAN DIV2K    & 23.3 M    & 20.7           & 0.067         
\end{tabular}
\end{table}

The results show that optimizing for PSNR is not the same as optimizing for SSIM in this scenario. The weather radar images' specificity may cause this, with their large areas of the same colour and sharp transitions between them.

SRGAN training worsened the generator network compared to the initial weights, both in metrics and visual perception. The discriminator network with $21.76$ M parameters is significantly bigger than the generator and was initialized randomly unlike the already trained SRResNet generator. I have not spent enough time tinkering the discriminator architecture, its initialization and generator loss parameters. Thus, the only conclusion drawn about SRGAN is that it is harder to train than traditional CNN.

While U-Net benefits from the DIV2K pre-training, SRResNet converges to roughly the same model in both scenarios, which may be caused by the larger number of trainable hyperparameters in U-Net.

\subsection{Video SR}
Based on the quantitative results and visual perception of the generated SR test set images, the target video SR was computed by both \textit{U-Net DIV2K} and \textit{SRResNet}. However, the results (Figure \ref{fig:frame}) are not satisfactory. Weather radar images are more specific than real-world photography data. Thus, the slight difference in images' structures from two different data sources may cause the noise in the SR frames.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- ZAVER
\section{Conclusion}

I have researched the use of CNN for weather radar video SR in this work. While the generated SR of the target video is not good enough, the weather radar test set evaluation hint that SR can be done on weather radar images.

Future work should include the following:
\begin{itemize}
  \item Experiment, whether SR from the training HR images can be computed. E.g. training on a video and upscaling the same video with the trained model.
  \item Use of perceptual losses to possibly improve the visual perception of the generated SR images.
  \item Further tinkering of the SRGAN training.
\end{itemize}

\onecolumn

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=.9\linewidth]{../imgs/unet_arch.png}\vskip-0.5cm
  \caption{Architecture of the U-Net network \cite{UnetCascade}.}
  \label{fig:unet}
\end{figure}

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=.9\linewidth]{../imgs/srgan_arch.png}\vskip-0.5cm
  \caption{Architectures of the generator and discriminator networks of SRGAN. Generator is also independent model SRResNet \cite{SRGAN}.}
  \label{fig:srgan}
\end{figure}

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=.9\linewidth]{../imgs/pred.png}\vskip-0.5cm
  \caption{Prediction from validation set by all methods.}
  \label{fig:pred}
\end{figure}

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=.9\linewidth]{../imgs/pred_small.png}\vskip-0.5cm
  \caption{Detail of prediction from validation set by all methods.}
  \label{fig:pred_small}
\end{figure}

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=.9\linewidth]{../imgs/frame_sr.png}\vskip-0.5cm
  \caption{Detail of target video SR.}
  \label{fig:frame}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Bibliography
%\bibliographystyle{plain-cz-online}
\bibliography{reference}

\end{document}