from core.utils.helpers import to_numpy

import numpy as np
from skimage.metrics import structural_similarity, mean_squared_error, peak_signal_noise_ratio

def mae(x, y):
    _x = to_numpy(x)
    _y = to_numpy(y)

    return 

def mse(x, y):
    _x = to_numpy(x)
    _y = to_numpy(y)
    
    return mean_squared_error(_x, _y)

def ssim(x, y):
    _x = to_numpy(x).reshape(-1, *x.shape[-2:]).transpose(1, 2, 0)
    _y = to_numpy(y).reshape(-1, *x.shape[-2:]).transpose(1, 2, 0)

    return structural_similarity(_x, _y, multichannel=True)

def psnr(x, y):
    _x = to_numpy(x)
    _y = to_numpy(y)
    
    return peak_signal_noise_ratio(_x, _y, data_range=255)