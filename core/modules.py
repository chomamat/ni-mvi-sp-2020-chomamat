import torch
import torch.nn as nn

from core.base import BaseModule, base_upsample

class BaseUpsampler(BaseModule):
    def __init__(self, lr_scale=4, upsample_mode='nearest', **kwargs):
        super().__init__(**kwargs)
        
        self.upsample = base_upsample(lr_scale=lr_scale, upsample_mode=upsample_mode)
        
        self.save_hyperparameters()
        
    def forward(self, x):
        return self.upsample(x)

class SRCNN(BaseModule):
    def __init__(self, lr_scale=4, batch_size=1, loss=nn.L1Loss(), val_loss=None):
        super().__init__(loss, val_loss)
        self.activation = nn.PReLU()

        self.first_upsample = nn.Upsample(scale_factor=lr_scale, mode='bicubic', align_corners=True)
        
        self.net = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=9, padding=4), self.activation,
            nn.Conv2d(64, 32, kernel_size=1), self.activation,
            nn.Conv2d(32, 3, kernel_size=5, padding=2)
        )
        
        self.save_hyperparameters()

    def forward(self, x):
        # upsample to real size
        u = self.first_upsample(x)
        
        # sr
        x = self.net(u)
        
        return torch.clip(x, -1, 1)
    
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

class UnetBT(BaseModule):
    def __init__(self, lr_scale=4, batch_size=1, loss=nn.L1Loss(), val_loss=None):
        super().__init__(loss, val_loss)
        self.activation = nn.PReLU()

        self.conv_setup = {
            'kernel' : (3,3),
            'stride' : (1,1),
            'padding' : 1,
            'activation' : self.activation
        }
        self.pooling_setup = {
            'kernel_size' : (2,2),
            'stride' : (2,2)
        }
        self.upsample_setup = {
            'scale_factor' : 2,
            'mode' : 'bicubic',
            'align_corners' : True
        }

        self.pooling_layer = nn.AvgPool2d(**self.pooling_setup)
        self.upsample_layer = nn.Upsample(**self.upsample_setup)
        self.first_upsample = nn.Upsample(scale_factor=lr_scale, mode='bicubic', align_corners=True)
        
        self.conv32 = self._convBlock(3, 32, **self.conv_setup)
        self.conv64 = self._convBlock(32, 64, **self.conv_setup)
        self.conv128 = self._convBlock(64, 128, **self.conv_setup)
        
        self.conv128_128 = self._convBlock(128, 128, kernel=(3, 3), stride=(1, 1), padding=1, activation=self.activation)

        self.upsample128 = self._upsampleBlock(self.upsample_layer, 128, 128, **self.conv_setup)

        self.deconv64_1 = nn.Conv2d(256, 128, kernel_size=(1, 1), stride=(1, 1))
        self.deconv64 = self._convBlock(128, 64, **self.conv_setup)
        self.upsample64 = self._upsampleBlock(self.upsample_layer, 64, 64, **self.conv_setup)

        self.deconv32_1 = nn.Conv2d(128, 64, kernel_size=(1, 1), stride=(1, 1))
        self.deconv32 = self._convBlock(64, 32, **self.conv_setup)
        self.upsample32 = self._upsampleBlock(self.upsample_layer, 32, 32, **self.conv_setup)

        self.deconv1_1 = nn.Conv2d(64, 32, kernel_size=(1, 1), stride=(1, 1))
        self.deconv1 = self._convBlock(32, 3, kernel=(3,3), stride=(1,1), padding=1, activation=None)
        
        self.save_hyperparameters()

    def forward(self, x):
        # upsample to real size
        u = self.first_upsample(x)
        
        # encode
        x32 = self.conv32(u)
        x32_p = self.pooling_layer(x32)
        x64 = self.conv64(x32_p)
        x64_p = self.pooling_layer(x64)
        x128 = self.conv128(x64_p)
        x128_p = self.pooling_layer(x128)

        x = self.conv128_128(x128_p)

        # decoding part
        x = self.upsample128(x)
        x = torch.cat((x, x128), dim=1)
        x = self.deconv64_1(x)
        x = self.deconv64(x)        
        
        x = self.upsample64(x)
        x = torch.cat((x, x64), dim=1)
        x = self.deconv32_1(x)
        x = self.deconv32(x)
        
        x = self.upsample32(x)
        x = torch.cat((x, x32), dim=1)
        x = self.deconv1_1(x)
        x = self.deconv1(x)

        return torch.clip(x, -1, 1)
    
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer
        
    @staticmethod
    def _convBlock(in_channels, out_channels, kernel, stride, padding, activation):
        net = nn.Sequential(
            nn.Conv2d(in_channels, in_channels, kernel, stride, padding), nn.PReLU(),
            nn.Conv2d(in_channels, in_channels, kernel, stride, padding), nn.PReLU(),
            nn.Conv2d(in_channels, out_channels, kernel, stride, padding)
        )
        if activation is not None:
            net = nn.Sequential(
                net, 
                nn.GroupNorm(out_channels//16, out_channels),
                activation
            )
        return net
    @staticmethod
    def _upsampleBlock(upsample, in_channels, out_channels, kernel, stride, padding, activation):
        return nn.Sequential(
            upsample,
            nn.Conv2d(in_channels, out_channels, kernel, stride, padding), nn.PReLU()
        )