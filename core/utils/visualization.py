import math
import matplotlib.pyplot as plt
import numpy as np

def _squeeze(x):
    _x = np.squeeze(x)
    while _x.ndim < 3:
        _x = _x[None, ...]
    return _x

def grid(X, cols=None, size=(15, 10), title=None, vmax=None, cmap=plt.cm.nipy_spectral):
    x = _squeeze(np.array(X))
    rows = math.ceil(len(x) / cols)

    fig, axes = plt.subplots(nrows=rows, ncols=cols,
                             figsize=(cols*size[0], rows*size[1]),
                             sharex=True, sharey=True)
    ax = axes.ravel()

    for i in range(rows):
        for j in range(cols):
            if i*cols + j < len(x):
                ax[i*cols + j].imshow(x[i*cols+j], cmap=cmap, vmax=vmax)
            else:
                break

    if title is not None:
        fig.suptitle(title, fontsize=30)
        fig.tight_layout(rect=[0, 0.03, 1, 0.93])
    else:
        fig.tight_layout()

def show(x, size=(15, 10), fig=True, title=None, vmax=None, cmap=plt.cm.nipy_spectral, **kwargs):
    if fig is True:
        plt.figure(figsize=size)
    
    if title is not None:
        plt.title(title)

    if (x.ndim == 2):
        return plt.imshow(x, cmap=cmap, vmax=vmax, **kwargs)
    else:
        _x = x.transpose(1, 2, 0)
        return plt.imshow(_x, **kwargs)

    plt.tight_layout()