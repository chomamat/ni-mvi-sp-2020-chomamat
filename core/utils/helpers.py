import json
import numpy as np
import torch

def to_numpy(x):
    if isinstance(x, np.ndarray):
        return x
    elif isinstance(x, torch.Tensor):
        return x.cpu().detach().numpy()
    else:
        raise NotImplementedError(
                f"Conversion from {type(x)} to numpy.ndarray not implemented.")

def torch_scale(x, from_, to):
    assert isinstance(from_, tuple) and len(from_) == 2
    assert isinstance(to, tuple) and len(to) == 2

    res = torch.clip(x, *from_)
    return (res - from_[0])/(from_[1] - from_[0])*(to[1] - to[0]) + to[0]

def dump_json(d, file):
    with open(file, 'w') as f:
        json.dump(d, f, indent=4)