import torch
import torch.nn as nn
import pytorch_lightning as pl

from core.utils.helpers import torch_scale

class BaseModule(pl.LightningModule):
    def __init__(self, loss=nn.L1Loss(), val_loss=None, range_in=(0, 1), range_out=(0, 1), range_base=(0, 255)):
        super().__init__()
        self.loss = loss
        self.val_loss = {} if val_loss is None else val_loss
        assert isinstance(self.val_loss, dict), 'val_loss has to be dict'
            
        self.range_in = range_in
        self.range_out = range_out
        self.range_base = range_base
        
        self.test_prefix = 'test_epoch'
            
    def predict(self, x, ret_np=True):
        _x = torch.tensor(x, device=torch.device('cuda' if self.on_gpu else 'cpu'))[None, ...].float()
        _x = torch_scale(_x, self.range_base, self.range_in)
        pred = self(_x)
        
        if ret_np is True:
            pred = torch_scale(pred, self.range_out, self.range_base).cpu().detach().numpy()[0]
            pred = pred.astype('uint8')
        
        return pred
    
    # --

    def training_step(self, train_batch, batch_idx):
        lr, hr = train_batch
        lr, hr = torch_scale(lr.float(), self.range_base, self.range_in), torch_scale(hr.float(), self.range_base, self.range_out)
        
        hr_hat = self(lr)
        loss = self.loss(hr_hat, hr)
        self.log('train/loss', loss)
        
        return loss
    
    def training_epoch_end(self, outputs):
        loss = torch.tensor([0], device=torch.device('cuda' if self.on_gpu else 'cpu')).float()
        for o in outputs:
            loss += o['loss']
        loss /= len(outputs)

        self.logger.log_metrics({'train/epoch_loss': loss}, step=self.current_epoch)
        
    # --

    def validation_step(self, val_batch, batch_idx):
        lr, hr = val_batch
        lr, hr = torch_scale(lr.float(), self.range_base, self.range_in), torch_scale(hr.float(), self.range_base, self.range_out)        
        
        hr_hat = self(lr)
        
        res = {'loss': self.loss(hr_hat, hr)}
        hr_hat, hr = torch_scale(hr_hat, self.range_out, self.range_base), torch_scale(hr, self.range_out, self.range_base)
        for key, loss in self.val_loss.items():
            res[key] = loss(hr_hat, hr)
        
        self.log('val_loss', res['loss'])
        
        return res
        
    def validation_epoch_end(self, outputs):
        res = {}
        for key in outputs[0].keys():
            loss = torch.tensor([0], device=torch.device('cuda' if self.on_gpu else 'cpu')).float()
            for o in outputs:
                loss += o[key]
            loss /= len(outputs)
            
            res[f'val_epoch/{key}'] = loss

        self.logger.log_metrics(res, step=self.current_epoch)
        
    # --
        
    def test_step(self, test_batch, batch_idx):
        lr, hr = test_batch
        lr, hr = torch_scale(lr.float(), self.range_base, self.range_in), torch_scale(hr.float(), self.range_base, self.range_out)        
        
        hr_hat = self(lr)
        
        res = {}
        hr_hat, hr = torch_scale(hr_hat, self.range_out, self.range_base), torch_scale(hr, self.range_out, self.range_base)
        for key, loss in self.val_loss.items():
            res[key] = loss(hr_hat, hr)
        
        return res
        
    def test_epoch_end(self, outputs):
        res = {}
        for key in outputs[0].keys():
            loss = torch.tensor([0], device=torch.device('cuda' if self.on_gpu else 'cpu')).float()
            for o in outputs:
                loss += o[key]
            loss /= len(outputs)

            res[f'{self.test_prefix}/{key}'] = loss

        self.logger.log_metrics(res, step=self.current_epoch)
        
# -----------------------------------------------------------------------------
        
def base_conv2d(in_channels, out_channels, kernel_size, stride=1, bias=True, act=None, bn=False):
    module_list = [nn.Conv2d(in_channels, out_channels, kernel_size,
        stride=stride, padding=(kernel_size//2), bias=bias)]
    if bn is True:
        module_list.append(nn.BatchNorm2d(out_channels))
    if act is not None:
        module_list.append(act)
        
    return nn.Sequential(*module_list)

def base_upsample(lr_scale=4, upsample_mode='nearest'):
    _corners = True if upsample_mode in ['linear', 'bilinear', 'bicubic', 'trilinear'] else None
    return nn.Upsample(scale_factor=lr_scale, mode=upsample_mode, align_corners=_corners)

class ResidualCatConv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        
        self.conv = base_conv2d(in_channels, out_channels, 1)
        
    def forward(self, *args):
        x = torch.cat(args, dim=1)
        return self.conv(x)