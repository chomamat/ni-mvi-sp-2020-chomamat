import torch
from torch import nn
from core.base import base_conv2d, BaseModule
from core.utils.helpers import torch_scale
import math

class ResBlock(nn.Module):
    def __init__(self, n_feat=64, kernel_size=3, act=nn.PReLU(), bn=True):
        super().__init__()

        module_list = []
        for i in range(2):
            module_list.append(base_conv2d(n_feat, n_feat, kernel_size, bn=bn, act=act if i == 0 else None))
        
        self.net = nn.Sequential(*module_list)
        
    def forward(self, x):
        y = self.net(x)
        
        return y + x
        
class SRGen(nn.Module):
    def __init__(self, n_feat=64, kernel_size=3, n_blocks=16, lr_scale=4, act=nn.PReLU(), bn=True):
        super().__init__()
        
        self.head = base_conv2d(3, n_feat, kernel_size*3, act=act)
        
        self.res_blocks = [ResBlock(n_feat=n_feat, kernel_size=kernel_size, act=act, bn=bn)
                           for _ in range(n_blocks)]
        self.res_blocks = nn.ModuleList(self.res_blocks)
        
        self.res_conv = base_conv2d(n_feat, n_feat, kernel_size, bn=bn)
        
        upsample = [nn.Sequential(
            base_conv2d(n_feat, n_feat*4, kernel_size),
            nn.PixelShuffle(2),
            act
        ) for _ in range(int(math.log(lr_scale, 2)))]
        self.upsample = nn.Sequential(*upsample)
        
        self.tail = base_conv2d(n_feat, 3, kernel_size*3)
        
    def forward(self, x):
        y_resid = y1 = self.head(x)
        
        for res_block in self.res_blocks:
            y_resid = res_block(y_resid)
        y_resid = self.res_conv(y_resid)
        y_resid += y1
        
        y_ups = self.upsample(y_resid)
        
        y = self.tail(y_ups)
        
        return y
    
class SRDisc(nn.Module):
    def __init__(self, W=128, n_feat=64, kernel_size=3, act=nn.LeakyReLU(0.2), bn=True):
        super().__init__()
        
        module_list = [
            base_conv2d(3, n_feat, kernel_size, act=act),
            base_conv2d(n_feat, n_feat, kernel_size, act=act, bn=bn)
        ]
        for i in range(3):
            module_list.append(base_conv2d(n_feat*2**i, n_feat*2**(i+1), kernel_size, act=act, bn=bn))
            module_list.append(base_conv2d(n_feat*2**(i+1), n_feat*2**(i+1), kernel_size, stride=2, act=act, bn=bn))
        module_list.append(base_conv2d(n_feat*2**(i+1), n_feat, kernel_size, act=act, bn=bn)) # sad GPU conv
        module_list.append(nn.Flatten())
        module_list.append(nn.Linear((W//2**3)**2*n_feat, 1024))
        module_list.append(act)
        module_list.append(nn.Linear(1024, 1))
        module_list.append(nn.Sigmoid())
        
        self.net = nn.Sequential(*module_list)
        
    def forward(self, x):
        return self.net(x)
    
class SRResNet(BaseModule):
    def __init__(self, n_feat=64, kernel_size=3, n_blocks=16, lr_scale=4, act=nn.PReLU(), bn=True, **kwargs):
        super().__init__(**kwargs)

        self.gen = SRGen(n_feat, kernel_size, n_blocks, lr_scale, act, bn)

        self.save_hyperparameters()

    def forward(self, x):
        return self.gen(x)
    
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-4)
        return optimizer
        
class SRGAN(BaseModule):
    def __init__(self, W=128, gen_init=None, n_feat=64, kernel_size=3, n_blocks=16, lr_scale=4, act=nn.PReLU(), bn=True,
                 loss=None, **kwargs):
        super().__init__(**kwargs)
        
        self.gen = SRGen(n_feat, kernel_size, n_blocks, lr_scale, act, bn)
        self.disc = SRDisc(W=W, n_feat=n_feat, kernel_size=kernel_size, bn=bn)
        
        # init generator
        if gen_init is not None:
            tmp_srresnet = SRResNet.load_from_checkpoint(gen_init)
            self.gen.load_state_dict(tmp_srresnet.gen.state_dict())
        
        self.mse = nn.MSELoss()
        
        self.save_hyperparameters()
        
    def forward(self, x):
        return self.gen(x)
    
    def gen_loss(self, lr, hr):
        hr_hat = self(lr)
        
        # content loss
        loss = self.mse(hr_hat, hr)
        
        # adversarial loss
        probs = self.disc(hr_hat)
        loss += 1e-3*torch.sum(-torch.log(probs))

        return loss
    
    def disc_loss(self, lr, hr):
        # train on real
        y_real = self.disc(hr)
        real_loss = -torch.sum(torch.log(y_real))
        
        # train on generated
        hr_hat = self(lr)
        y_gen = self.disc(hr_hat)
        gen_loss = -torch.sum(torch.log(1 - y_gen))
        
        return real_loss + gen_loss
    
    def training_step(self, train_batch, batch_idx, optimizer_idx):
        lr, hr = train_batch
        lr, hr = torch_scale(lr.float(), self.range_base, self.range_in), torch_scale(hr.float(), self.range_base, self.range_out)

        # train generator
        result = None
        if optimizer_idx == 0:
            result = self.gen_loss(lr, hr)
            self.log('train/g_loss', result)

        # train discriminator
        if optimizer_idx == 1:
            result = self.disc_loss(lr, hr)
            self.log('train/d_loss', result)
            
        return result
    
    def training_epoch_end(self, outputs):
        return
            
    def validation_step(self, val_batch, batch_idx):
        lr, hr = val_batch
        lr, hr = torch_scale(lr.float(), self.range_base, self.range_in), torch_scale(hr.float(), self.range_base, self.range_out)        
        
        hr_hat = self(lr)

        res = {'loss': self.mse(hr_hat, hr)} # {'loss': self.gen_loss(lr, hr)}
        hr_hat, hr = torch_scale(hr_hat, self.range_out, self.range_base), torch_scale(hr, self.range_out, self.range_base)
        for key, loss in self.val_loss.items():
            res[key] = loss(hr_hat, hr)

        self.log('val_loss', res['loss'])

        return res

    def configure_optimizers(self):
        opt_g = torch.optim.Adam(self.gen.parameters(), lr=1e-4)
        opt_d = torch.optim.Adam(self.disc.parameters(), lr=1e-4)
        return [opt_g, opt_d], []